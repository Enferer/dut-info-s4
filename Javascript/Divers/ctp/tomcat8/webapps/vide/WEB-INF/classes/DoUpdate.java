// Servlet Test.java  de test de la configuration
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/DoUpdate")
public class DoUpdate extends HttpServlet
{
	Connection con;
	
  public void service( HttpServletRequest req, HttpServletResponse res ) 
       throws ServletException, IOException
  {
		PrintWriter out = res.getWriter();

	  
	try {
		Class.forName("org.postgresql.Driver");

		String url = "jdbc:postgresql://psqlserv/n3p1";

		String nom = "quentind";
		String mdp = "moi";
		con = DriverManager.getConnection(url,nom,mdp);

		res.setContentType("text/html;charset=UTF-8");
		out.println( "<head><title>servlet first</title></head><body><center>" );
		out.println( "<META content=\"charset=UTF-8\">"
				+ "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				+ "<link rel=\"stylesheet\""
				+ "href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\"> </head><body><center>" );
		Statement state = con.createStatement();
		
	ResultSet result = state.executeQuery("Select * from "+req.getParameter("table")+";");
		out.println("Select * from "+req.getParameter("table")+";");
		ResultSetMetaData resultMeta = result.getMetaData();
		String query="";
		query = "Update "+req.getParameter("table")+ " set ";
		out.println(query);
	    for(int i =2; i<=resultMeta.getColumnCount();i++){
	    if(resultMeta.getColumnType(i) != Types.INTEGER){
	    	query+=resultMeta.getColumnName(i)+ " = '"+req.getParameter(resultMeta.getColumnName(i))+"'  "; 
	    }
	    else{
	    	query+=resultMeta.getColumnName(i)+ " = "+req.getParameter(resultMeta.getColumnName(i))+" "; 

	    }
	    if(i<resultMeta.getColumnCount()){
	    	query += " , ";
	    }
	    	
	    }
	    query+=" where "+ resultMeta.getColumnName(1) + " = '"+req.getParameter("cle")+"';" ;
	    out.println(query );	

	    
		try{
			state.executeUpdate(query);
		}catch(Exception e){
			out.println(e.getMessage());
		}
			
		out.println("</center> </body>");
		

	} catch (Exception e) {
		out.println(e.getMessage());
	}finally {
		try {		
	con.close();} catch (SQLException e) {}
	}
	res.sendRedirect("Select?table="+req.getParameter("table"));
	    
    
  }
}
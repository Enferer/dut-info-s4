package tpDAO;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


	@WebServlet("/servlet/Catalogue")
	public class Catalogue extends HttpServlet
	{
		public void service( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
		{
			res.setContentType("text/html;charset=UTF-8");
			PrintWriter out = res.getWriter();
			out.println( "<head><title>servlet first</title></head><body><center>" );
			out.println( "<META content=\"charset=UTF-8\">"
					+ "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
					+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
					+ "<link rel=\"stylesheet\""
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' integrity='sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb' crossorigin='anonymous'> </head><body><center>" );
			ProductDAO dao = new ProductDAO();
			if(req.getParameter("fno") != null ){
				out.println("size : " + dao.findByFNO(Integer.valueOf(req.getParameter("fno"))).size() );
				out.println(ProductView.getHtml(dao.findByFNO(Integer.valueOf(req.getParameter("fno")))));

			}
			else{
				out.println(ProductView.getHtml(dao.findAll()));
			}
			out.println("</center></body>");
			
			
			
		}
	}
	


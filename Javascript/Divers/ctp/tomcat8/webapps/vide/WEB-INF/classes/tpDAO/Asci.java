package tpDAO;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;



@WebServlet("/servlet/Asci42")
public class Asci extends HttpServlet
{
	
	public void service( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		out.println( "<head><title>servlet first</title></head><body><center>" );
		out.println( "<META content=\"charset=UTF-8\"></head><body><center>" );
		out.println( "<table border=2>" );
		int nbMax = Integer.valueOf(req.getParameter("nbCol"));
		
		int cpt=0;
		out.println("<tr>");
		for(int i =32;i<256;i++){
			out.println( " <td>" + (char) i + "</td><td>"+ i+"</td> ");
			cpt++;
			if(cpt==nbMax){
				cpt=0;
				out.println("</tr><tr>");
				
			}
		}
		
		out.println("</tr></table>");
//		out.println("<p>"+ req.getParameter("nbCol")+"</p>");
		out.println("</center></body>");
	}
}
		
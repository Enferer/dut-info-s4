import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import java.sql.*;
import java.util.ArrayList;



@WebServlet("/servlet/ListerB")
public class ListerB extends HttpServlet
{
	public void service( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		out.println( "<head><title>servlet first</title></head><body><center>" );
		out.println( "<META content=\"charset=UTF-8\"></head><body><center>" );
		
//		out.println("<p>"+ req.getParameter("nbCol")+"</p>");
		
		Connection con=null;
	      Statement stmt;
	try{
		
		// enregistrement du driver
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		 // connexion à la base
		  String url = "jdbc:odbc:PostgreSQL35W";
		  String nom = "quentind";
		  
		  String mdp = "moi";
		   con = DriverManager.getConnection(url,nom,mdp);
	
      
		stmt = con.createStatement();
		String parametre = req.getParameter("tri");
		String param2 = req.getParameter("sens");
		String query = "select nom,annee,nationalite,categ,club,temps from resultats ";
		
		if(parametre != null && paramNonValide(parametre)){
			query += "order by " + parametre;
		}
		if(param2 != null && (param2.equals("asc") || param2.equals("desc"))){
			query += " "+ param2;
		}
		   query+=";";
		out.println("<form action = 'ListerB' method = 'get'>" 
				+ "<label for='tri'> Tri : </label> <input type='text' size='20' name='tri' ><br>" +
					"<label for='sens'> Sens : </label> <input type='text' size='4' name='sens'><br> "+
					"<Input type=submit value =Envoyer> </form>");
		
		
		out.println("<p>"+query+ "ok"+"</p>");					
		ResultSet rs = stmt.executeQuery(query);
		ResultSetMetaData meta = rs.getMetaData();
		
		out.println("<table border=2> <tr>");
		for(int i =1;i<=meta.getColumnCount();i++){
			if(param2==null || param2.equals("desc")){	
				out.println("<td> <a href=ListerA?tri=" + meta.getColumnName(i)
					+"&sens=asc > "+ meta.getColumnName(i)+"</a> </td>");
			}
			else{
				out.println("<td> <a href=ListerA?tri=" + meta.getColumnName(i)
					+"&sens=desc > "+ meta.getColumnName(i)+"</a> </td>");
			}
		
		}
		
		out.println("</tr>");
		
      while (rs.next()) 
		{
		  String n = rs.getString(1); // nom
		  int a = rs.getInt(2);       // année
		  String nationalite = rs.getString(3); // nationalite
		String categ = rs.getString(4); // categ
		String c = rs.getString(5); // club
		int te = rs.getInt(6); // temps



		  
		  out.println("<tr> <td>"+ n+"</td><td>"+a+"</td><td>"+
			nationalite+"</td> <td>"+categ+"</td> <td>"+c+"</td> <td>"+te+"</td></tr>");
			
		
		}

	}
	catch(Exception e2){
      System.out.println(e2.getMessage());	}
	finally{
		try{con.close();}catch(Exception e42){};
	}
	out.println("</table></center></body>");
	}
	
	public boolean paramNonValide(String s){
		ArrayList<String> valide = new ArrayList<>();
		valide.add("nom");
		valide.add("annee");
		valide.add("nationalite");
		valide.add("categ");
		valide.add("club");
		valide.add("temps");
		return valide.contains(s);
	}
}
		
package ctp2;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/servlet/Enregistrer")
public class Enregistrer extends HttpServlet {
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		Connection con =null;
		Statement stmt;
		try{
			
			// enregistrement du driver
			  Class.forName("org.postgresql.Driver");
			 // connexion � la base
			  String url = "jdbc:postgresql://psqlserv/n3p1";
			  
			  String nom = "quentind";
			  String mdp = "moi";
			   con = DriverManager.getConnection(url,nom,mdp);

			stmt = con.createStatement();
			int max = 1;
			String qmax = "Select max(mno) from Menu ";
			ResultSet rs = stmt.executeQuery(qmax);
			rs.next();
			if(rs.getString(1)!=null) {
				max = Integer.valueOf(rs.getString(1));
			}
			max++;
			String query = "insert into Menu values ( " + max + " , "+ session.getAttribute("E")+ " , "+session.getAttribute("P")+ " , "
					+ session.getAttribute("D") + " , '" + req.getParameter("mail")+ "' , '" + req.getParameter("datepicker")+ "' ) ;";
			System.out.println(query);
			con.createStatement().executeUpdate(query);
			session.removeAttribute("E");
			session.removeAttribute("P");
			session.removeAttribute("D");
		res.sendRedirect("../plats.jsp?type=E");
		
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}

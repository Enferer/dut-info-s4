package tpDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FournisseurDAO {
	private Connection con;
	
	private void getConnection() {
		try {
			Class.forName("org.postgresql.Driver");
		
		// connexion � la base
		String url = "jdbc:postgresql://psqlserv/n3p1";

		String nom = "quentind";
		String mdp = "moi";
		con = DriverManager.getConnection(url, nom, mdp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	public ArrayList<Fournisseur> findAll(){
		ArrayList<Fournisseur> list = new ArrayList<>();
		String query = "Select * from fournisseur;";
		getConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()){
				Fournisseur temp = new Fournisseur(rs.getInt(1), rs.getString(2), rs.getString(3));
				list.add(temp);
			}
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} 
		finally{
			try{con.close();}catch(Exception e42){System.out.println(e42.getMessage());	};
		}
		return list;
	}
	
	
	public Fournisseur findById(int id){

		ArrayList<Fournisseur> list = findAll();
		for(Fournisseur p : list){
			if(p.getFno() == id){
				return p;
			}
		}
		
		return null;
	}
	
}

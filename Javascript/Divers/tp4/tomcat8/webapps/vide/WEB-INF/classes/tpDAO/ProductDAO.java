package tpDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProductDAO {

	private Connection con;
	private FournisseurDAO fourn;
	
	public ProductDAO(){}

	public ProductDAO(FournisseurDAO fourn) {
		super();
		this.fourn = fourn;
	}

	private void getConnection() {
		try {
			Class.forName("org.postgresql.Driver");
		
		// connexion � la base
		String url = "jdbc:postgresql://psqlserv/n3p1";

		String nom = "quentind";
		String mdp = "moi";
		con = DriverManager.getConnection(url, nom, mdp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	public ArrayList<Product> findAll(){
		ArrayList<Product> list = new ArrayList<>();
		FournisseurDAO fournDAO = new FournisseurDAO();
		String query = "Select * from produits;";
		
		
		getConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()){
				Fournisseur tempf = null;
				for(Fournisseur f :  fournDAO.findAll()){
					if(f.getFno() == rs.getInt(6)){
						tempf= f;
					}
				}
				Product temp = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5),tempf);
				list.add(temp);
			}
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} 
		finally{
			try{con.close();}catch(Exception e42){System.out.println(e42.getMessage());	};
		}
		return list;
	}
	
	
	public Product findById(int id){

		ArrayList<Product> list = findAll();
		for(Product p : list){
			if(p.getPno() == id ){
				return p;
			}
		}
		
		return null;
	}
	
	public ArrayList<Product> findByFNO(int id){
		
		ArrayList<Product> list = findAll();
		ArrayList<Product> list2 = new ArrayList<>();

		for(Product p : list){
			if(p.getFourn().getFno() == id ){
				list2.add(p);
			}
		}
		return list2;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

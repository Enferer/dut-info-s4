package tpDAO;

public class Fournisseur {
	private int fno;
	private String nom;
	private String location;
	
	public Fournisseur(int fno, String nom, String location) {
		super();
		this.fno = fno;
		this.nom = nom;
		this.location = location;
	}

	public int getFno() {
		return fno;
	}

	public String getNom() {
		return nom;
	}

	public String getLocation() {
		return location;
	}
	
	
	
	
}

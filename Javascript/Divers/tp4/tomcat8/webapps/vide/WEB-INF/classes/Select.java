  import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/servlet/Select")
public class Select extends HttpServlet
{
	public void service( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		
		
		
		//Dans le select il y a aussi le saisie
		
		
		res.setContentType("text/html;charset=UTF-8");
		PrintWriter out = res.getWriter();
		out.println( "<head><title>servlet first</title></head><body><center>" );
		out.println( "<META content=\"charset=UTF-8\">"
				+ "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				+ "<link rel=\"stylesheet\""
				+ "href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\"> </head><body><center>" );
		
//		out.println("<p>"+ req.getParameter("nbCol")+"</p>");
		
		Connection con=null;
	      Statement stmt;
	try{
		
		// enregistrement du driver
		  Class.forName("org.postgresql.Driver");
		 // connexion � la base
		  String url = "jdbc:postgresql://psqlserv/n3p1";
		  
		  String nom = "quentind";
		  String mdp = "moi";
		   con = DriverManager.getConnection(url,nom,mdp);

      
		stmt = con.createStatement();
		String query = "select * from "+  req.getParameter("table")+";";
		out.println("<p>"+query+"</p>");
		ResultSet rs = stmt.executeQuery(query);
		ResultSetMetaData meta = rs.getMetaData();
		out.println("<table class= \"table table-hover\"> ");
		out.println("<tr>");
		for(int i =1;i<=meta.getColumnCount();i++){
			out.println("<td>"+meta.getColumnName(i)+"</td>");
		}
		out.println("</tr><tr>");
      while (rs.next()) 
		{
    	  
    	  for(int i =1;i<= meta.getColumnCount();i++){
    		  out.println("<td>"+ rs.getString(i)+"</td>");
    	  }
    	  out.println("<td><a href='DoDelete?table="+req.getParameter("table")+"&cle="+rs.getString(1)+"'>Delete</a></td>" );
    	  out.println("<td><a href='Update?table="+req.getParameter("table")+"&cle="+rs.getString(1)+"'>Update</a></td>" );

    	  out.println("</tr>");
		  
			
		
		}
      
      out.println("<form action='Saisie' method='get'><tr>  ");
      for(int i =1; i<=meta.getColumnCount();i++){
    	out.println("<td><label>"+meta.getColumnName(i)+"</label>");
    	out.println("<input type='text' name='champ"+i+"'></td>");  
      }
		out.println("<input type='hidden' value='"+req.getParameter("table")+"' name='table'>"); 

      out.println("</tr></table><input type='submit' value='Submit'></form>");
  	out.println("</center></body>");

	}
	
	
	
	
	catch(Exception e2){
      out.println(e2.getMessage());	}
	finally{
		try{con.close();}catch(Exception e42){out.println(e42.getMessage());	};
	}
	}
}
		
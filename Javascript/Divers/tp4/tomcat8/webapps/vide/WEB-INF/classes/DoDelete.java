// Servlet Test.java  de test de la configuration
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/servlet/DoDelete")
public class DoDelete extends HttpServlet
{
	Connection con;
	
  public void service( HttpServletRequest req, HttpServletResponse res ) 
       throws ServletException, IOException
  {
		PrintWriter out = res.getWriter();

	  
	try {
		Class.forName("org.postgresql.Driver");

		String url = "jdbc:postgresql://psqlserv/n3p1";

		String nom = "quentind";
		String mdp = "moi";
		con = DriverManager.getConnection(url,nom,mdp);

		res.setContentType("text/html;charset=UTF-8");
		out.println( "<head><title>servlet first</title></head><body><center>" );
		out.println( "<META content=\"charset=UTF-8\">"
				+ "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				+ "<link rel=\"stylesheet\""
				+ "href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\"> </head><body><center>" );
		Statement state = con.createStatement();
		
		ResultSet result = state.executeQuery("Select * from "+req.getParameter("table")+";");
		ResultSetMetaData resultMeta = result.getMetaData();
		String query = "Delete from "+req.getParameter("table") + " where "+resultMeta.getColumnName(1)+ "='" + req.getParameter("cle")+ "';";
		out.println("<p>"+query+"</p>");
		try{
			state.executeUpdate(query);
		}catch(Exception e){
			out.println(e.getMessage());
		}
			
		out.println("</center> </body>");
		

	} catch (Exception e) {
		out.println(e.getMessage());
	}finally {
		try {		
	con.close();} catch (SQLException e) {}
	}
	res.sendRedirect("Select?table="+req.getParameter("table"));
	    
    
  }
}

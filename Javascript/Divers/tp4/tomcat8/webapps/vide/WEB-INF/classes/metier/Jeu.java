package metier;

import java.util.Scanner;

public class Jeu {
	
	
	
	public static void main(String [] arg){
		JeuPileOuFace j= new JeuPileOuFace();
		Scanner sc = new Scanner(System.in);
		j.init();
		
		while(!j.termine()){
			System.out.println("Anciens coups : " + j.getHumain().toString());
			String enJeu="";
			System.out.println("Que voulez vous jouer ? ");
			do{
				
				enJeu = sc.nextLine();
			}while(enJeu.charAt(0) != 'P' && enJeu.charAt(0) != 'F');
			j.play(enJeu.charAt(0));
			
			System.out.println("Point(s) humain : " + j.getPointsHumain() + " et point(s) IA : "+ j.getPointsOrdi() );
			
			
			
		}
		if(j.getPointsHumain() == 10){
			System.out.println("Humain gagne ");
		}
		else{
			System.out.println("IA gagne");
		}
	}
	
}

package ctp2;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/servlet/Choix")
public class Choix extends HttpServlet {
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		session.setAttribute(req.getParameter("type"), req.getParameter("choix"));
		
		System.out.println("Le choix est : "+ req.getParameter("choix"));
		res.sendRedirect("../plats.jsp?type="+req.getParameter("type"));
		
		
	}
}

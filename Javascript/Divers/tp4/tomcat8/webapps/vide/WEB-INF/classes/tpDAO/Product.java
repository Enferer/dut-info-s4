package tpDAO;

public class Product {

	private int pno;
	private String libelle;
	private String desc ; 
	private String urlImg;
	private int prixHT;
	private Fournisseur fourn ;
	
	
	
	
	public Fournisseur getFourn() {
		return fourn;
	}
	public Product(int pno, String libelle, String desc, String urlImg, int prixHT) {
		super();
		this.pno = pno;
		this.libelle = libelle;
		this.desc = desc;
		this.urlImg = urlImg;
		this.prixHT = prixHT;
	}
	public Product(int pno, String libelle, String desc, String urlImg, int prixHT,Fournisseur f) {
		super();
		this.pno = pno;
		this.libelle = libelle;
		this.desc = desc;
		this.urlImg = urlImg;
		this.prixHT = prixHT;
		fourn=f;
	}
	public int getPno() {
		return pno;
	}
	public String getLibelle() {
		return libelle;
	}
	public String getDesc() {
		return desc;
	}
	public String getUrlImg() {
		return urlImg;
	}
	public int getPrixHT() {
		return prixHT;
	}
	@Override
	public String toString() {
		return "Product [fno = "+ this.fourn.getFno()+"pno=" + pno + ", libelle=" + libelle + ", desc=" + desc + ", urlImg=" + urlImg + ", prixHT="
				+ prixHT + "]";
	}
	
	
	
	
	
	
}

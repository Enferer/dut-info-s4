package tpDAO;

import java.util.ArrayList;

public class ProductView {

	public static String getHtml(ArrayList<Product> list){
		
		String res="";
		res += " <table class='table table-striped table-dark'> ";
		for(Product p : list){
			res += "<tr><td> <a href='Produit?id="+p.getPno()+" '>"+p.getLibelle()+"</a></td><td>"+p.getPrixHT()+"€ </td><td> <img src='"+p.getUrlImg()+"' width=100 height=100 </td></tr>" ;
		}
		res += "</table>";
		return res;
	}
	
	public static String getDetail(Product p ){
		String res ="<table class='table table-hover'> <tr><td>Libelle</td><td>Prix</td><td>image</td><td>Desc</td><td>pno</td>     </tr><tr class='bg-info'>";
		res +=  "<td> "+p.getLibelle()+"</td><td>"+p.getPrixHT()+"€ </td><td> <img src='"+p.getUrlImg()+"' width=200 height=200 </td>" ;
		res += "<td>"+ p.getDesc() + "</td><td>"+ p.getPno() + "</td><td><a href='Catalogue?fno="+p.getFourn().getFno()+"'> "+p.getFourn().getFno()+"</a></td></tr></table>";
		
		
		return res;
	}
}

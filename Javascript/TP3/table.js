var table = $('#table1');

table.html("");

var cpt = 0;

for ( var lig = 0; lig < 8; lig++) {
	var row = $('<tr></tr>');
	for (var col = 0; col < 8; col++) {
		$('<td>'+cpt+'</td>').appendTo(row);
		cpt++;
	}
	row.appendTo(table);
}
var monde;

var mondeVide = function(nbLig, nbCol, funcVal){
    var arr = [[]];
    for (var x = 0; x < nbCol; ++x) {
        for (var y = 0; y < nbLig; ++y) {
            if (arr[y] == null) arr[y] = [];
            arr[y][x] = funcVal();
        }
    }
    return arr;
}

var width = 50;
var height = 30;
monde = mondeVide(height, width, function(){return (Math.random() < 0.2 ? 1 : 0);});
var mondeTMP = mondeVide(height,width,function(){return 0;});
var affiche = function() {
   var table = $('#table');

   table.html("");

   var cpt = 0;

   for ( var lig = 0; lig < height; lig++) {
    var row = $('<tr></tr>');
    for (var col = 0; col < width; col++) {
        var etat = monde[lig][col] == 0 ? "dead" : "alive";
        $('<td class="'+etat+'"> </td>').appendTo(row);
        cpt++;
    }
    row.appendTo(table);
}
}

var getCell = function(lig, col) {
    if (lig < 0) return height-1;
    if (col < 0) return width-1;
    return monde[lig%height][col%width];
}



var voisins = function(lig, col) {
    var s = 0;
    if (getCell(lig-1, col-1) == 1) ++s;
    if (getCell(lig, col-1) == 1) ++s;
    if (getCell(lig+1, col-1) == 1) ++s;  

    if (getCell(lig-1, col) == 1) ++s;
    if (getCell(lig+1, col) == 1) ++s;

    if (getCell(lig-1, col+1) == 1) ++s;
    if (getCell(lig, col+1) == 1) ++s;
    if (getCell(lig+1, col+1) == 1) ++s;

    return s;
}
var evolution = function(vivante, nbVoisins) {
    if (nbVoisins == 3) return 1;
    if (nbVoisins == 2) return vivante;
    return 0;
}

var simule = function() {
    mondeTMP = mondeVide(height, width, function(){return 0;});
    for (var x = 0; x < width; ++x) {
        for (var y = 0; y < height; ++y) {
            mondeTMP[y][x] = evolution(monde[y][x], voisins(y, x));
        }
    }
    monde = mondeTMP;
}

var run = function() { setInterval(function() {affiche(monde); simule();}, 100)};
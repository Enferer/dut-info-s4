var monde = randomWorld(25, 50);

function randomWorld(nbLig, nbCol){
  var tab = [];
  for (var i = 0; i < nbLig; i++) {
    if (tab[i] == null) tab[i] = [];
    for (var j = 0; j < nbCol; j++) {
      var value = Math.random();
      if (value < 0.8) 
        tab[i][j] = 0;
      else 
        tab[i][j] = 1;
    }
  }
  return tab;
};


var mondeVide = function(nbLig, nbCol, value){
  var tab = [];//new Array(nbLig, nbCol);
  for (var i = 0; i < nbLig; i++) {
    if (tab[i] == null) tab[i] = [];
    for (var j = 0; j < nbCol; j++) {
      tab[i][j] = value;
    }
  }
  return tab;
};
var mondeTMP = mondeVide(25,50,0);
var affiche = function() {
  var screen = document.getElementById("GOLScreen");
  screen.style['font-family'] = 'monospace';
  screen.innerHTML = '';
  for (var idxL=0; idxL<monde.length; idxL++) {
     var ligne = "";
     for (var idxC=0; idxC<monde[0].length; idxC++) {
       ligne += (monde[idxL][idxC] === 0 ? '.' : '*');
     }
     var node = document.createTextNode(ligne);
     screen.appendChild(node);
     var newLine = document.createElement("br");
     screen.appendChild(newLine);
  }
}
var voisins = function(lig, col) {
    var voisin = 0;
    for (var lign = lig-1; lign <= lig+1; lign++ ) {
      for (var colo = col-1; colo <= col+1; colo++) {
          if (lign != lig || colo != col) {
            voisin+=monde[lign][colo];    
          }
      }
    }
    return voisin;
};
var evolution = function(vivante, nbVoisins) {
  if (vivante == 1 && (nbVoisins == 2 ||nbVoisins == 3))
      return 1;
  else if (vivante == 0 && nbVoisins == 3)
      return 1;
  else 
      return 0;  

}
    
var simule = function() {
    for (var i = 1; i < monde[1].length-1; i++) {
      for (var j = 1; j < monde.length-1; j++) {
        mondeTMP[i][j] = evolution(monde[i][j], voisins(i, j));
      }
    }

    monde = mondeTMP;
    mondeTMP = mondeVide(50, 50, 0);
};


function run() {
  for (var i=0; i<10; i++) {
    setTimeout(function() {affiche(monde); simule(); }, 1000*i);
  }
}


affiche();

function next() {
  simule();
  affiche();
}

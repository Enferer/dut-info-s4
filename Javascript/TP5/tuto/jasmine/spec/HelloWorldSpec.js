describe("Hello world", function() {
  it("says hello", function() {
    expect(helloWorld()).toEqual("Hello world!");
  });
});

describe("Divisible", function() {
	
	beforeEach(function () {
		jasmine.addMatchers({
			toBeDivisibleByTwo: function() {
				return (this.actual % 2) == 0;
			}
		});
	});

	it ('is Divisible', function() {
		expect(gimmeANumber()).toBeDivisibleByTwo();
	});

});
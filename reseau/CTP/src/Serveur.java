import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Serveur {

	
	ServerSocket ss;
	Socket client;
	InputStream in;
	OutputStream out;
	int port = 4241;
	
	
	public Serveur(int port) {
		try {
			this.ss = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public void run() throws IOException {
		
		String reponse;
		int idc;
		while(true) {
			client = ss.accept();
			
			out = client.getOutputStream();
			in = client.getInputStream();
			
			byte[] buf = new byte[3];
			in.read(buf);
			String msg = new String(buf);
			switch (msg.toLowerCase()) {
			case "udp":
				this.port++;
				idc = new Random().nextInt(100);
				reponse = idc+":"+"udp:"+this.port;
				ThreadUDP t = new ThreadUDP(idc,this.port);
				t.start();
				out.write(reponse.getBytes());
				client.close();
				break;
			case "tcp":
				this.port++;
				idc = new Random().nextInt(100);
				reponse = idc+":"+"udp:"+this.port+"\n";
				ThreadTCP tcp = new ThreadTCP(idc,this.port);
				tcp.start();
				out.write(reponse.getBytes());
				client.close();

			default:
				break;
			}
		}


	}
	
	public static void main(String[] args) {
		Serveur s = new Serveur(12345);
		try{s.run();}
		catch (Exception e) {e.printStackTrace();}
	}
	
}

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ThreadUDP extends Thread{

	private DatagramSocket dgSocket;
	private int idc;
	
	public ThreadUDP(int idc,int port) {
		
		try {
			
			this.dgSocket = new DatagramSocket(port);
			this.idc = idc;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("catch ");
		}
		
	}
	
	private void dialogue() {
		byte[] buf = new byte[106];
		DatagramPacket dgPacket = new DatagramPacket(buf, 106);
		String reponse;
		while(true) {
			try{
				
				this.dgSocket.receive(dgPacket);
				String msg = new String(dgPacket.getData());
				if(msg.equals(String.valueOf(this.idc))) {
					dgSocket.close();
					return;
				}
				
				String[] tab = msg.split(":");
				
				int idc = Integer.valueOf(tab[0]);
				if(idc != this.idc) return;
				
				int shift = Integer.valueOf(tab[1]);
				String messageRecu = tab[2];
				
				reponse = CesarTools.transform(messageRecu, shift);
				dgPacket.setData(reponse.getBytes());
				dgSocket.send(dgPacket);
				
			}catch (Exception e) {
				e.printStackTrace();
				System.out.println("ERREUR : Déconnection");
				this.dgSocket.close();
				return;
			}
		}
		
	}
	
	public void run() {
		dialogue();
		try {
			dgSocket.close();
		} catch(Exception e) {e.printStackTrace();}
	}
	


	
}

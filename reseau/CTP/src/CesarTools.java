/**
 * @author J. Carle, IUT-A, Université de Lille
 * 
 * @version 0.1
 * 
 * @note Méthode nécessaire pour le protocole César.
 */
public final class CesarTools {
	public static String transform(String str, int shift) {
		String result = "";
		for (char c : str.toCharArray())
			result += transform(c, shift);
		return result;
	}

	private static char transform(char c, int shift) {
		int startVal;
		if (('a' <= c) && (c <= 'z'))
			startVal = 'a';
		else if (('A' <= c) && (c <= 'Z'))
			startVal = 'A';
		else
			return c;
		return (char) ((c - startVal + (shift < 0 ? shift + 26 : shift)) % 26 + startVal);
	}
}
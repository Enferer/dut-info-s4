

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Client {

	private Socket socket;
	private BufferedWriter writer;
	private BufferedInputStream reader;
	private String host;
	private int port;
	private String user;
	private Dialogue d;

	public Client(String host, int port, String user) {
		this.host = host;
		this.port = port;
		this.user = user;
		d = new Dialogue(this);
	}

	public Client(String host, int port) {
		this(host, port, "anonymous");
	}

	public void connection() throws IOException {
		this.socket = new Socket(host, port);
		this.reader = new BufferedInputStream(socket.getInputStream());
		this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		System.out.println(read());
		send("USER " + user);
		System.out.println(read());
		String passwd = "";
		send("PASS " + passwd);
		System.out.println(read());
		d.go();
	}	

	// lecture de la reponse du serveur
	protected String read() {
		String reponse = "";
		int stream = 0;
		byte[] b = new byte[4096];
		
		try {
			stream = reader.read(b);
		} catch (Exception e) {e.printStackTrace();}
		
		reponse = new String(b, 0, stream);
		return reponse;
	}

	// envoi d'une commande au serveur
	protected void send(String commande){
		commande += "\r\n";

		try {
			System.out.println("La commande envoyé au serv : " + commande);
			writer.write(commande);
			writer.flush();
		} catch (Exception e){
			System.out.println("Error envoi de la commande au "
					+ "serveur (write, fonction send(String commande): " + e.toString());
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws IOException {

		Client client = new Client("localhost", 2121);
		client.connection();
}
}

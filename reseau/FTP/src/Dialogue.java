

import java.util.Scanner;

public class Dialogue {

	private Client client;
	private String reponse;
	private boolean quit = false;

	public Dialogue(Client client) {
		this.client = client;
	}

	public void go() {

		System.out.println("-- Connecté --");
		while(!quit) {
			Scanner sc = new Scanner(System.in);
			reponse = sc.nextLine().toUpperCase();
			System.out.println(getCommand(reponse));
		}
	}

	public String getCommand(String reponseClient) {
		
		String arg1 = "";
		//String arg2 = "";
		String cmd = "";
		
		String serveurReponse = "";
		String tab[] = reponseClient.split(" ");
		cmd = tab[0];
		
		if(tab.length > 1) arg1 = tab[1];
		//if(tab.length > 2) arg2 = tab[2];
		
		switch (cmd) {
		case "PWD":
			serveurReponse =  pwd();
			break;
		case "NOOP":
			serveurReponse =  noop();
			break;
		case "CWD":
			serveurReponse =  cwd(arg1);
			break;
		case "CDUP":
			serveurReponse =  cdup();
			break;
		case "QUIT":
			serveurReponse =  quit();
			this.quit = false;
			break;
		case "RETR":
			serveurReponse =  retr(arg1);
			break;
		case "MODE":
			serveurReponse =  mode(arg1.charAt(0));
			break;
		case "PORT":
			serveurReponse =  port(arg1);
			break;
		case "PASV":
			serveurReponse =  pasv();
			break;
		case "STOR":
			serveurReponse =  stor(arg1);
			break;
		case "LIST":
			serveurReponse =  list(arg1);
			break;
		case "HELP":
			serveurReponse =  help(arg1);
			break;
		case "TYPE":
			System.out.println("todo --> essayez une autre commande");
			/*if(tab.length > 2) {
				if(arg2.length() == 1)
					serveurReponse =  type(arg1.charAt(0), arg2.charAt(0));
				else
					serveurReponse =  type(arg1.charAt(0), Integer.parseInt(arg2));
			} else {
				serveurReponse =  type(arg1.charAt(0));
			}
			break;*/
		default:
			serveurReponse = "cette commande n'existe pas";
			break;
		}
		return serveurReponse;
	}
	
	
	public String pwd() {
		this.client.send("PWD");
		return client.read();
	}

	public String cwd(String dir) {
		this.client.send("CWD " + dir);
		return client.read();
	}

	public String cdup() {
		this.client.send("CDUP");
		return client.read();
	}

	public String quit() {
		this.client.send("QUIT");
		return client.read();
	}

	public String retr(String dir) {
		this.client.send("RETR " + dir);
		return client.read();
	}

	public String stor(String dir) {
		this.client.send("STOR " + dir);
		return client.read();
	}

	public String list(String dir) {
		this.client.send("LIST " + dir);
		return client.read();
	}

	public String help(String cmd) {
		this.client.send("HELP " + cmd);
		return client.read();
	}

	public String noop() {
		this.client.send("NOOP");
		return client.read();
	}

	public String type(char t) {
		this.client.send("TYPE " + t);
		return client.read();
	}

	public String type(char type1, char type2) {
		this.client.send("TYPE " + type1 + " " + type2);
		return client.read();
	}

	public String type(char l, int sizeOctets) {
		this.client.send("TYPE " + l + " " + sizeOctets);
		return client.read();
	}

	public String stru(char s) {
		this.client.send("STRU " + s);
		return client.read();
	}

	public String mode(char m) {
		this.client.send("MODE " + m);
		return client.read();
	}

	public String port(String port) {
		this.client.send("PORT " + port);
		return client.read();
	}

	public String pasv() {
		this.client.send("PASV");
		return client.read();
	}	

}

package ctp;

/**
 * M4102 : client TCP
 * @author <a href="mailto:jean.carle@univ-lille1.fr">Jean Carle</a>, IUT-A, Universite de Lille 1
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientC {

	Socket client;
	BufferedReader fromSrv;

	public ClientC() throws UnknownHostException, IOException {
		this("localhost", 19);
	}

	public ClientC(String serveur, int portDst) throws UnknownHostException, IOException {
		client = new Socket(serveur, portDst);
		fromSrv = new BufferedReader(new InputStreamReader(client.getInputStream()));
	}

	private void go() throws IOException {
		while(true) {
			System.out.println(fromSrv.readLine());
		}
		
	}

	public static void main(String[] args) throws NumberFormatException, UnknownHostException, IOException {
		switch (args.length) {
		case 0:
			new ClientC().go();
			break;
		case 2:
			new ClientC(args[0], Integer.parseInt(args[1])).go();
			break;
		default:
			System.err.println("Erreur : 0 ou 2 arguments");
		}
	}
}
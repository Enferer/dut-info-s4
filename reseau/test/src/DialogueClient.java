package reseau_FTP;

import java.util.Scanner;

public class DialogueClient {

	private Command command;
	private String reponseClient;
	private Scanner sc = new Scanner(System.in);
	private boolean run = true;

	public DialogueClient(Command command) {
		this.command = command;
	}

	public void dialogue() {

		System.out.println("*********************************************\n");
		System.out.println("    Vous êtes maintenant connecté au FTP    ");
		System.out.println("\n*********************************************");

		while(run) {
			sc.reset();
			System.out.print("Entrez votre commande : ");
			reponseClient = sc.nextLine().toUpperCase();
			System.out.println(choseCommande(reponseClient));
		}
	}

	public String choseCommande(String reponseClient) {
		String reponseServeur = "";
		String commandes[] = reponseClient.split(" ");
		String arg1 = "";
		String arg2 = "";

		System.out.println("La commande est : " + commandes[0]);
		if(commandes.length > 1) {
			arg1 = commandes[1];
			if(commandes.length > 2) {
				arg2 = commandes[2];
			}
		}

		try {
			switch (commandes[0]) {
			case "PWD":
				reponseServeur = command.pwd();
				break;
			case "CWD":
				if(commandes.length < 2 )
					arg1 = needArgument();
				reponseServeur = command.cwd(arg1);
				break;
			case "CDUP":
				reponseServeur = command.cdup();
				break;
			case "QUIT":
				reponseServeur = command.quit();
				run = false;
				break;
			case "RETR":
				if(commandes.length < 2 )
					arg1 = needArgument();
				reponseServeur = command.retr(arg1);
				break;
			case "STOR":
				if(commandes.length < 2 )
					arg1 = needArgument();
				reponseServeur = command.stor(arg1);
				break;
			case "LIST":
				reponseServeur = command.list(arg1);
				break;
			case "HELP":
				reponseServeur = command.help(arg1);
				break;
			case "NOOP":
				reponseServeur = command.noop();
				break;
			case "TYPE":
				if(commandes.length > 2) {
					if(arg2.length() == 1)
						reponseServeur = command.type(arg1.charAt(0), arg2.charAt(0));
					else
						reponseServeur = command.type(arg1.charAt(0), Integer.parseInt(arg2));
				} else {
					if(commandes.length < 2 )
						arg1 = needArgument();
					reponseServeur = command.type(arg1.charAt(0));
				}
				break;
			case "MODE":
				if(commandes.length < 2 )
					arg1 = needArgument();
				reponseServeur = command.mode(arg1.charAt(0));
				break;
			case "PORT":
				if(commandes.length < 2 )
					arg1 = needArgument();
				reponseServeur = command.port(arg1);
				break;
			case "PASV":
				reponseServeur = command.pasv();
				break;
			default:
				reponseServeur = "Commande inconnue !";
				break;
			}
		} catch (Exception e) {
			System.out.println("Erreur saisie commande : " + e.toString());
			reponseServeur = "Erreur d'argument dans la commande.";
		}
		return reponseServeur;
	}

	private String needArgument() {
		String arg;
		System.out.println("Il manque un argument a votre commande.");
		System.out.print("Votre argument : ");
		arg = sc.nextLine();
		System.out.println();
		return arg;
	}
}

package reseau_FTP;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Client {

	private Socket socket;
	private BufferedWriter writer;
	private BufferedInputStream reader;
	private String host;
	private int port;
	private String user;
	private DialogueClient dialogue_Client;

	public Client(String host, int port, String user) {
		this.host = host;
		this.port = port;
		this.user = user;
		dialogue_Client = new DialogueClient(new Command(this));
	}

	public Client(String host, int port) {
		this(host, port, "anonymous");
	}

	public void connection() throws IOException {
		try {
			socket = new Socket(host, port);
		} catch (Exception e) {
			System.out.println("Erreur connextion serveur : " + e.toString());			
		}

		try {
			reader = new BufferedInputStream(socket.getInputStream());
		} catch (IOException e) {
			System.out.println("Error initialisation reader (BufferedInputStream) : " + e.toString());
			e.printStackTrace();
		}

		try {
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		} catch (IOException e) {
			System.out.println("Error initialisation writer (BufferedWriter) : " + e.toString());
			e.printStackTrace();
		}

		String reponse = read();

		if(!reponse.startsWith("220")){
			throw new IOException("Erreur de connexion au FTP : \n" + reponse);
		}

		send("USER " + user);
		reponse = read();

		if(!reponse.startsWith("331"))
			throw new IOException("Erreur de connexion avec le compte utilisateur : \n" + reponse);

		String passwd = "";
		send("PASS " + passwd);
		reponse = read();

		if(!reponse.startsWith("230"))
			throw new IOException("Erreur de connexion avec le compte utilisateur : \n" + reponse);

		dialogue_Client.dialogue();
	}	

	// lecture de la reponse du serveur
	protected String read() {
		String reponse = "";
		int stream = 0;
		byte[] b = new byte[4096];

		try {
			stream = reader.read(b);
		} catch (IOException e) {
			System.out.println("Error lecture de la réponse du serveur "
					+ "(reader, fonction read()) : " + e.toString());
			e.printStackTrace();
		}

		reponse = new String(b, 0, stream);
		return reponse;
	}

	// envoi d'une commande au serveur
	protected void send(String commande){
		commande += "\r\n";

		try {
			System.out.println("La commande envoyé au serv : " + commande);
			writer.write(commande);
			writer.flush();
		} catch (Exception e){
			System.out.println("Error envoi de la commande au "
					+ "serveur (write, fonction send(String commande): " + e.toString());
			e.printStackTrace();
		}
	}
}

package reseau_FTP;

public class Command {

	private Client client;

	public Command(Client client) {
		this.client = client;
	}

	public String pwd() {
		//On envoie la commande
		client.send("PWD");
		//On lit la réponse
		return client.read();
	}

	public String cwd(String dir) {
		client.send("CWD " + dir);
		return client.read();
	}

	public String cdup() {
		client.send("CDUP");
		return client.read();
	}

	public String quit() {
		client.send("QUIT");
		return client.read();
	}

	public String retr(String dir) {
		client.send("RETR " + dir);
		return client.read();
	}

	public String stor(String dir) {
		client.send("STOR " + dir);
		return client.read();
	}

	public String list(String dir) {
		client.send("LIST " + dir);
		return client.read();
	}

	public String help(String cmd) {
		client.send("HELP " + cmd);
		return client.read();
	}

	public String noop() {
		client.send("NOOP");
		return client.read();
	}

	public String type(char type) {
		client.send("TYPE " + type);
		return client.read();
	}

	public String type(char typeA, char typeB) {
		client.send("TYPE " + typeA + " " + typeB);
		return client.read();
	}

	public String type(char l, int sizeOctets) {
		client.send("TYPE " + l + " " + sizeOctets);
		return client.read();
	}

	public String stru(char stru) {
		client.send("STRU " + stru);
		return client.read();
	}

	public String mode(char mode) {
		client.send("MODE " + mode);
		return client.read();
	}

	// port format : PORT h1,h2,h3,h4,p1,p2
	public String port(String port) {
		client.send("PORT " + port);
		return client.read();
	}

	public String pasv() {
		client.send("PASV");
		return client.read();
	}	
}

/*
 * Depommier Tanguy Groupe S
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;


public class ServeurTCP {


	private ServerSocket socketServer;

	public ServeurTCP() throws IOException {
		// On lance le serveur sur un port libre
		socketServer = new ServerSocket(0);
		new MyThread(socketServer).start();
	}

	public int getPort() {
		return socketServer.getLocalPort();
	}


	
	/**
	 * Classe permettant de renvoyer la citation au client
	 */
	class MyThread extends Thread {

		private ServerSocket socketServer;

		public MyThread(ServerSocket s) {
			this.socketServer = s;
		}


		public void run() {
			Socket client;
			try {
				client = socketServer.accept();
				Writer out = new OutputStreamWriter(client.getOutputStream());
				out.write(readFile());

				out.close();
				client.close();
				socketServer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		private String readFile() {
			String res = "";

			BufferedReader br = null;

			try {

				String sCurrentLine;

				br = new BufferedReader(new FileReader("humoristes"));


				// On lit tout le fichier
				while ((sCurrentLine = br.readLine()) != null) {
					res+=sCurrentLine+"\n";
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}

			String []quote = res.split("%");

			// On renvoit  une citation aleatoire du fichier
			return quote[getRandom(quote.length)];

		}

		private int getRandom(int max) {
			Random r = new Random();
			return r.nextInt(max);
		}

	}

}





package ex1;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class InterfaceList {


	public static void main(String[] args) throws SocketException {
		Enumeration<NetworkInterface> enu = NetworkInterface.getNetworkInterfaces();
		
		while (enu.hasMoreElements()) {
			System.out.println(showInfo(enu.nextElement()));
		}
		
	}
	
	public static String showInfo(NetworkInterface ni) throws SocketException {
		String res = new String();
		
		res+= ni.getName()+" : \n";
		
		for (InterfaceAddress ia : ni.getInterfaceAddresses()) {
			res+= ia.toString()+"\n";
		}
		
		res+="Taille MTU : "+ni.getMTU()+"\n";
		
		return res;
	}

}

package ex2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ClientUDP {

	
	public static void main(String[] args) throws UnknownHostException, IOException {
		
		DatagramSocket dg = new DatagramSocket();
		
		DatagramPacket dp = new DatagramPacket(new byte[20], 20, InetAddress.getByName("localhost"), 9876);
		
		dg.send(dp);
		dg.receive(dp);
		
		System.out.println(new String(dp.getData(), 0, dp.getLength()));
		
		dg.close();
	}

}

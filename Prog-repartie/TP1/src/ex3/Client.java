package ex3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		DatagramSocket dg = new DatagramSocket();
		DatagramPacket sendPacket = new DatagramPacket(new byte[256], 256, InetAddress.getByName("localhost"), 9876);
		DatagramPacket receivedPacket = new DatagramPacket(new byte[256], 256, InetAddress.getByName("localhost"), 9876);


		
		String message = args[0];
		
		sendPacket.setData(message.getBytes());
	

		dg.send(sendPacket);
		
		dg.receive(receivedPacket);
		
		System.out.println(new String(receivedPacket.getData(), 0, 256));

		
		dg.close();
	}

}

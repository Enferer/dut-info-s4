package ex3_4;


import java.io.*;
import java.net.*;

public class ClientEchoMT {
	public static final int PORT_SERVICE = 9876;
	private ServerSocket s_Srv;

	ClientEchoMT() throws IOException {
		s_Srv = new ServerSocket(PORT_SERVICE);
	}

	// Réception clients et transfert vers un thread dédié
	private void attenteClient() throws IOException {
		Socket s_Clt;
		while(true) {
			s_Clt = s_Srv.accept();
			new ReponseTruc(s_Clt).start();
		}
	}

	// Test d'usage de la classe ... et rien d'autre
	public static void main(String [] args) throws IOException {
		ClientEchoMT srvTruc = new ClientEchoMT();
		srvTruc.attenteClient();
	}

	/**
	 * Gestion du protocole du service <<Truc>>
	 **/
	class ReponseTruc extends Thread {
		private Socket s_Client;
		private DataOutputStream out;
		private BufferedReader in;

		ReponseTruc(Socket sClient) {
			this.s_Client = sClient;
			try {
				this.in = new BufferedReader(new InputStreamReader(s_Client.getInputStream()));
				this.out = new DataOutputStream(s_Client.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		void dialogue () { 
			String buf;
			try {
				while((buf = in.readLine()) != null) {
					out.writeBytes(buf+"\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void run() {
			dialogue();

			try {
				s_Client.close();
				out.close();
				in.close();
			} catch(IOException e) {}
		}
	}
}
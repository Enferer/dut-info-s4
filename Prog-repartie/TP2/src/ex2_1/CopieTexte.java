package ex2_1;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class CopieTexte {

	public CopieTexte(String in, String out) throws IOException {
		FileReader fin = null;
		FileWriter fout = null;
		
		try {
			fin = new FileReader(in);
			fout = new FileWriter(out);
			
			char []buf = new char[1024];
			while(fin.read(buf) > 0) {
				fout.write(buf);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fin.close();
			fout.close();
		}
	}
	
}

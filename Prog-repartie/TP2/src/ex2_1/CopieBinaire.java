package ex2_1;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class CopieBinaire {

	public CopieBinaire(String in, String out) throws IOException {
		
		FileInputStream fin = null;
		FileOutputStream fout = null;
		
		try {
		fin = new FileInputStream(in);
		fout = new FileOutputStream(out);
		
		byte []buffer = new byte[1024];
		int length;
		
		while((length = fin.read(buffer)) > 0) {
			fout.write(buffer, 0, length);
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fin.close();
			fout.close();
		}
	}
	
}

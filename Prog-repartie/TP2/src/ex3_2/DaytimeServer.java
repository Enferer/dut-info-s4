package ex3_2;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class DaytimeServer {


	public DaytimeServer() throws IOException {

		ServerSocket s = null;

		try {
			s = new ServerSocket(9876);

			while(true) {
				Socket client = s.accept();		
				Writer out = new OutputStreamWriter(client.getOutputStream());

				Date d = new Date();
				out.write(d.toString()+"\n");

				out.close();
				client.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			s.close();
		}
	}


	public static void main(String[] args) throws IOException {
		new DaytimeServer();
	}

}

package ex3_3;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class EchoServer {

	
	
	public EchoServer() throws IOException {
		ServerSocket server = null;
		
		try {
			server = new ServerSocket(9876);
			
			while (true) {
				Socket client = server.accept();
				
				DataOutputStream out = new DataOutputStream(client.getOutputStream());
				BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
				
				Date d = new Date();

				out.writeBytes(d.toString()+"\n");
				
				String buf;
				while((buf = in.readLine()) != null) {
					out.writeBytes(buf+"\n");
				}
				
				out.close();
				in.close();
				client.close();

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			server.close();
		}
	}
	
	public static void main(String[] args) throws IOException {
		new EchoServer();
	}

}

package ex2_4;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientEcho {

	
	public ClientEcho(String host, int port) throws Exception {
		
		Socket s = null;
		
		try {
			s = new Socket(host, port);
			
			BufferedReader inUser = new BufferedReader(new InputStreamReader(System.in));
			BufferedReader inServer = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			DataOutputStream out = new DataOutputStream(s.getOutputStream());

			System.out.println("Debut de la communication.");
			
			String inClient;
			String buf = new String();
			System.out.print("Vous : ");
			while (!(inClient = inUser.readLine()).equals("FIN") && !inClient.equals(".")) {
				out.writeBytes(inClient+"\n");
				buf = inServer.readLine();
				System.out.println("Serveur : "+buf);
				System.out.print("Vous : ");
			}
			
			System.out.println("Fin de la communcation.");
			
			inUser.close();
			inServer.close();
			out.close();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			s.close();
		}
		
		
	}
	
	
	
	public static void main(String[] args) throws Exception {
		new ClientEcho("localhost", 9876);
	}

}

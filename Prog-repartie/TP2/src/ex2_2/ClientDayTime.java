package ex2_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientDayTime {

	public ClientDayTime(String serveur, int port) throws IOException {
		Socket s = null;
		try {
			
			// On se connecte au serveur
			s = new Socket(serveur, port);

			// On lit ce que le serveur renvoit
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

			// Tant que le serveur renvoie qq chose on l'affiche
			while (!in.ready()) {}
			System.out.println(in.readLine()); // Read one line and output it

			in.close();



		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			s.close();
		}
	}

	public static void main(String []args) throws IOException {
		@SuppressWarnings("unused")
		ClientDayTime cdt = new ClientDayTime("localhost", 9876);
	}

}

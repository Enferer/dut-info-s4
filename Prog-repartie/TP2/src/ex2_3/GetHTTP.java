package ex2_3;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class GetHTTP {


	public GetHTTP(String host, int port) throws IOException {
		Socket s = null;

		try {
			s = new Socket(host, port);

			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			DataOutputStream out = new DataOutputStream(s.getOutputStream());

			String request = "GET / HTTP/1.0\n\n";

			out.writeBytes(request);
			
			

			String buf;
			boolean print = false;
			while ((buf = in.readLine()) != null) {
				if (print) System.out.println(buf); // Read one line and output it
				if (buf.length() == 0) print = true;
			}
			in.close();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			s.close();
		}
	}
	
	public static void main(String []args) throws IOException {
		new GetHTTP("localhost", 80);
	}

}

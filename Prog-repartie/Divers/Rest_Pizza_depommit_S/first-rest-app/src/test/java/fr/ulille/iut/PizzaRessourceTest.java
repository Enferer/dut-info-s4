package fr.ulille.iut;

import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Path("pizza")
public class PizzaRessourceTest {

	private HttpServer server;
	private WebTarget target;

	@Before
	public void setUp() throws Exception {
		// start the server
		server = Main.startServer();
		// create the client
		Client c = ClientBuilder.newClient();

		target = c.target(Main.BASE_URI);

		// Utiliser un attribut statique pour stocker les pizzas dans la ressource PizzaResource
		// est une très mauvaise pratique. Cela pose problème pour les tests.
		// Du coup, je suis obligé de remettre à zéro cet attribut "à la main"...
		Field field = PizzaRessource.class.getDeclaredField("pizzas");
		field.setAccessible(true);
		field.set("null", new HashMap<>());
	}

	@SuppressWarnings("deprecation")
	@After
	public void tearDown() throws Exception {
		server.stop();
	}

	/**
	 * Test de création d'un utilisateur (retour HTTP et envoi de l'URI de la nouvelle instance)
	 */
	@Test
	public void testCreatePizza() {
		Pizza pizza = new Pizza("Regina", "Tomate", 7);
		// Conversion de l'instance de Pizza au format JSON pour l'envoi
		Entity<Pizza> pizzaEntity = Entity.entity(pizza, MediaType.APPLICATION_JSON);

		// Envoi de la requête HTTP POST pour la création de l'utilisateur
		Response response = target.path("/pizza").request().post(pizzaEntity);

		// Vérification du code de retour HTTP
		assertEquals(201, response.getStatus());

		// Vérification que la création renvoie bien l'URI de la nouvelle instance dans le header HTTP 'Location'
		// ici : http://localhost:8080/myapp/users/jsteed
		URI uriAttendue = target.path("/pizza").path(pizza.getName()).getUri();
		assertTrue(uriAttendue.equals(response.getLocation()));
	}

	/**
	 * Test de création en double d'une pizza. Doit renvoyer 409
	 */
	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza("4 Fromages", "Crème", 7);
		Entity<Pizza> pizzaEntity = Entity.entity(pizza, MediaType.APPLICATION_JSON);

		// Envoi de la requête HTTP POST pour la création de la pizza
		int first = target.path("/pizza").request().post(pizzaEntity).getStatus();

		// Vérification du code de retour HTTP
		assertEquals(201, first);

		int same = target.path("/pizza").request().post(pizzaEntity).getStatus();
		assertEquals(409, same);
	}

	/**
	 * Vérifie qu'initialement on a une liste de pizza vide
	 */
	@Test
	public void testGetEmptyListofPizzas() {
		List<Pizza> list = target.path("/pizza").request().get(new GenericType<List<Pizza>>(){});
		assertTrue(list.isEmpty());
	}

	/**
	 * Vérifie que je renvoie bien une liste contenant toutes les pizza (ici 2)
	 */
	@Test
	public void testGetTwoPizzas() {
		Pizza pizza1 = new Pizza("Du Ch'Nord", "Crème", 7);
		Entity<Pizza> pizzaEntity = Entity.entity(pizza1, MediaType.APPLICATION_JSON);

		target.path("/pizza").request().post(pizzaEntity);

		Pizza pizza2 = new Pizza("Charcutière", "Tomate",7);
		pizzaEntity = Entity.entity(pizza2, MediaType.APPLICATION_JSON);

		target.path("/pizza").request().post(pizzaEntity);

		List<Pizza> list = target.path("/pizza").request().get(new GenericType<List<Pizza>>(){});
		assertEquals(2, list.size());
	}

	/**
	 * Vérifie la récupération d'une pizza spécifique
	 */
	@Test
	public void testGetOnePizza() {
		Pizza pizza = new Pizza("Kebab", "Creme", 7);
		Entity<Pizza> pizzaEntity = Entity.entity(pizza, MediaType.APPLICATION_JSON);
		target.path("/pizza").request().post(pizzaEntity);

		Pizza result = target.path("/pizza").path("Kebab").request().get(Pizza.class);
		assertEquals(pizza, result);
	}

	/**
	 * Vérifie que la récupération d'une pizza inexistant renvoie 404
	 */
	@Test
	public void testGetInexistantPizza() {
		int notFound = target.path("/pizza").path("tking").request().get().getStatus();
		assertEquals(404, notFound);
	}

	/**
	 *
	 * Vérifie que la suppression d'une ressource est effective
	 */
	@Test
	public void testDeleteOnePizza() {
		Pizza pizza = new Pizza("Original", "Creme", 7);
		Entity<Pizza> pizzaEntity = Entity.entity(pizza, MediaType.APPLICATION_JSON);
		target.path("/pizza").request().post(pizzaEntity);

		int code = target.path("/pizza").path("Original").request().delete().getStatus();
		assertEquals(204, code);

		int notFound = target.path("/pizza").path("Original").request().get().getStatus();
		assertEquals(404, notFound);  
	}

	/**
	 *
	 * Vérifie que la suppression d'une pizza inexistant renvoie 404
	 */
	@Test
	public void testDeleteInexistantPizza() {
		int notFound = target.path("/pizza").path("tking").request().delete().getStatus();
		assertEquals(404, notFound);
	}

	/**
	 *
	 * Vérifie que la modification d'un utilisateur inexistant renvoie 404
	 */
	@Test
	public void testModifyInexistantPizza() {
		Pizza inexistant = new Pizza("La Pizza Qui Existent Pas", "Inexistance", 0);
		Entity<Pizza> pizzaEntity = Entity.entity(inexistant, MediaType.APPLICATION_JSON);
		int notFound = target.path("/pizza").path("La Pizza Qui Existent Pas").request().put(pizzaEntity).getStatus();
		assertEquals(404, notFound);
	}

	/**
	 *
	 * Vérifie que la modification d'une ressource est effective
	 */
	@Test
	public void testModifyPizza() {
		Pizza pizza = new Pizza("Regina", "Tomateee", 18);
		Entity<Pizza> pizzaEntity = Entity.entity(pizza, MediaType.APPLICATION_JSON);
		target.path("/pizza").request().post(pizzaEntity);

		Pizza modified = new Pizza("Regina", "Tomate", 7);
		pizzaEntity = Entity.entity(modified, MediaType.APPLICATION_JSON);

		int noContent = target.path("/pizza").path("Regina").request().put(pizzaEntity).getStatus();
		assertEquals(204, noContent);

		Pizza retrieved = target.path("/pizza").path("Regina").request().get(Pizza.class);
		assertEquals(modified, retrieved);
	}

	/**
	 *
	 * Vérifie la prise en charge de l'encodage application/x-www-form-urlencoded
	 */
	@Test
	public void testCreatePizzaFromForm() {
		Form form = new Form();
		form.param("name", "Jambon");
		form.param("base", "Tomate");
		form.param("prix", "7");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		int code = target.path("/pizza").request().post(formEntity).getStatus();

		assertEquals(201, code);
	}

	/**
	 * Vérifie qu'on récupère bien un utilisateur avec le type MIME application/xml
	 */
	@Test
	public void testGetPizzaAsXml() {
		Pizza pizza = new Pizza("Regina", "Tomate", 7);
		Entity<Pizza> pizzaEntity = Entity.entity(pizza, MediaType.APPLICATION_JSON);
		target.path("/pizza").request().post(pizzaEntity);

		int code = target.path("/pizza").path("Regina").request(MediaType.APPLICATION_XML).get().getStatus();
		assertEquals(200, code);
	}

}

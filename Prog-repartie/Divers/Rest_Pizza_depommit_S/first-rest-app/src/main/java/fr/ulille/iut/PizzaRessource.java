package fr.ulille.iut;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Context;

import java.net.URI;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Ressource Pizza (accessible avec le chemin "/pizza")
 */
@Path("pizza")
public class PizzaRessource { 
	// Pour l'instant, on se contentera d'une variable statique pour conserver l'état
	private static Map<String, Pizza> pizzas = new HashMap<>();

	// L'annotation @Context permet de récupérer des informations sur le contexte d'exécution de la ressource.
	// Ici, on récupère les informations concernant l'URI de la requête HTTP, ce qui nous permettra de manipuler
	// les URI de manière générique.
	@Context
	public UriInfo uriInfo;

	/**
	 * Une ressource doit avoir un contructeur (éventuellement sans arguments)
	 */
	public PizzaRessource() {
	}

	/**
	 * Méthode de création d'un utilisateur qui prend en charge les requêtes HTTP POST
	 * La méthode renvoie l'URI de la nouvelle instance en cas de succès
	 *
	 * @param  pizza Instance d'utilisateur à créer
	 * @return Response le corps de la réponse est vide, le code de retour HTTP est fixé à 201 si la création est faite
	 *         L'en-tête contient un champs Location avec l'URI de la nouvelle ressource
	 */
	@POST
	public Response createPizza(Pizza pizz) {
		// Si l'utilisateur existe déjà, renvoyer 409
		if ( pizzas.containsKey(pizz.getName())) {
			return Response.status(Response.Status.CONFLICT).build();
		}
		else {
			pizzas.put(pizz.getName(), pizz);

			// On renvoie 201 et l'instance de la ressource dans le Header HTTP 'Location'
			URI instanceURI = uriInfo.getAbsolutePathBuilder().path(pizz.getName()).build();
			return Response.created(instanceURI).build();
		}
	}

	/**
	 * Method prenant en charge les requêtes HTTP GET.
	 *
	 * @return Une liste de pizzas.
	 */
	@GET
	public List<Pizza> getPizza() {
		return new ArrayList<Pizza>(pizzas.values());
	}

	/** 
	 * Méthode prenant en charge les requêtes HTTP GET sur /pizza/{name}
	 *
	 * @return Une instance de Pizza
	 */
	@GET
	@Path("{name}")
	@Produces({"application/json", "application/xml"})
	public Pizza getPizza(@PathParam("name") String name) {
		// Si la pizza est inconnu, on renvoie 404
		if (  ! pizzas.containsKey(name) ) {
			throw new NotFoundException();
		}
		else {
			return pizzas.get(name);
		}
	}

	@DELETE
	@Path("{name}")
	public Response deletePizza(@PathParam("name") String name) {
		// Si la pizza est inconnu, on renvoie 404
		if (  ! pizzas.containsKey(name) ) {
			throw new NotFoundException();
		}
		else {
			pizzas.remove(name);
			return Response.status(Response.Status.NO_CONTENT).build();
		}
	}

	/** 
	 * Méthode prenant en charge les requêtes HTTP DELETE sur /pizza{name}
	 *
	 * @param name le nom de la pizza à modifier
	 * @param pizza l'entité correspondant à la nouvelle instance
	 * @return Un code de retour HTTP dans un objet Response
	 */
	@PUT
	@Path("{name}")
	public Response modifyPizza(@PathParam("name") String name, Pizza pizza) {
		// Si l'utilisateur est inconnu, on renvoie 404
		if (  ! pizzas.containsKey(name) ) {
			throw new NotFoundException();
		}
		else{
			pizzas.put(pizza.getName(), pizza);
			return Response.status(Response.Status.NO_CONTENT).build();
		}
	}

	/**
	 * Méthode de création d'une pizza qui prend en charge les requêtes HTTP POST au format application/x-www-form-urlencoded
	 * La méthode renvoie l'URI de la nouvelle instance en cas de succès
	 *
	 * @param name le nom de la pizza
	 * @param base la base de la pizza
	 * @param composants les composants de la pizza
	 * @param prix le prix de la pizza
	 * @return Response le corps de la réponse est vide, le code de retour HTTP est fixé à 201 si la création est faite
	 *         L'en-tête contient un champs Location avec l'URI de la nouvelle ressource
	 */
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name, @FormParam("base") String base,
			@FormParam("prix") int prix) {
		// Si l'utilisateur existe déjà, renvoyer 409
		if ( pizzas.containsKey(name) ) {
			return Response.status(Response.Status.CONFLICT).build();
		}
		else {
			pizzas.put(name, new Pizza(name, base, prix));

			// On renvoie 201 et l'instance de la ressource dans le Header HTTP 'Location'
			URI instanceURI = uriInfo.getAbsolutePathBuilder().path(name).build();
			return Response.created(instanceURI).build();
		}
	}

}

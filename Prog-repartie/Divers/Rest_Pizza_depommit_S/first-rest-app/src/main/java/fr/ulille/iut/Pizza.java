package fr.ulille.iut;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pizza {
	private String name;
	private String base;
	private int prix;

	public Pizza(String n, String b, int p) {
		this.name = n;
		this.base = b;
		this.prix = p;
	}

	public Pizza() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean equals(Object p) {
		return name.equals(((Pizza) p).name) || base.equals(((Pizza) p).base) 
				 || prix == ((Pizza) p).prix;
	}

	public String toString() {
		return "Pizza : " + name + "\nBase : " + base + "\nPrix : " + prix;
	}
}

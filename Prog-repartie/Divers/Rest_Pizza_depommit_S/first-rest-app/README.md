# DEPOMMIER Thibaut S

## Représentation JSON

Pizza:

    {
    "nom" : nom,
    "description" : description,
    "prix" : prix
    }

Commande:

	{
	"nom_client": nom,
	"list_pizza": [ { nom:"nom", description:"description", prix:"prix"}, ],
	"prix_total": prix,
	"id": id
	}


## Définition de l'API de la ressource Commande


| `Opération` | `URI` | `Acion réalisée` | `Retour` |
| -------------- | ---------- | -------------- | ------------ |
| **GET** | /commandes | récupère l'ensemble des commandes | 200 et un tableau des commandes |
| **GET** | /commandes/{id} | récupère la commande d'identifiant id | 200 et la commande <br> 404 si l'id n'éxiste pas  |
| **POST** | /commandes | création d'une commande | 201 et l'URI de la ressource qui a été crée <br> 400 si les éléments ne sont pas correct <br> 409 si la commande a déja était crée |
| **PUT** | /commandes/{id} | modification d'une commande | 200 et la commande qui a été modifiée <br> 400 si les informations ne sont pas correctes <br> 404 si l'id n'est pas connu  |
| **DELETE** | /commandes/{id} | delete de la commande de grace à sont id | 204 si la commande est correct <br> 404 si l'id n'est pas connu |



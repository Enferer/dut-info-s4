DEPOMMIER Thibaut S

## I] Découpage d'URI

* a.

	1. http://www.univ-lille1.fr/Accueil/Contacts/Services+étudiants/
		 
		 schema 	: http
		 host  		: www.univ-lille1.fr 
		 port  		: (80)
		 path  		: Accueil/Contacts/Services+étudiants/
		 query  	: none
		 fragment  	: none

	2. https://tools.ietf.org/html/rfc3986
		 
		 schema 	: https
		 host  		: tools.ietf.org
		 port  		: (80)
		 path  		: html/rfc3986
		 query  	: none
		 fragment  	: none

	3. http://localhost:8080/users/1
		 
		 schema 	: http
		 host  		: localhost
		 port  		: 8080
		 path  		: html/rfc3986
		 query  	: none
		 fragment  	: none

	4. https://fr.wikipedia.org/wiki/Markdown#Titres
		 
		 schema 	: https
		 host  		: fr.wikipedia.org
		 port  		: (80)
		 path  		: wiki/Markdown
		 query  	: none
		 fragment  	: Titres

	5. http://traduction.culturecommunication.gouv.fr/url/Result.aspx?to=en&url=http%3A%2F%2Fwww.culturecommunication.gouv.fr%2F
		 
		 schema 	: http
		 host  		: traduction.culturecommunication.gouv.fr
		 port  		: (80)
		 path  		: url/Result.aspx
		 query  	: to=en&url=http%3A%2F%2Fwww.culturecommunication.gouv.fr%2F
		 fragment  	: Titres

* b.

	http%3A%2F%2Fwww.culturecommunication.gouv.fr%2F = http://www.culturecommunication.gouv.fr/
    
## 2] Analyse de requête HTTP

* a.

	1. http://graphql.org/code/
	2. http://httpbin.org/post?foo=bar&toto=tutu

* b.
 
	1. code 201 = code de succès de création , donc la méthode PUT est possible
	2. l'effet sur le serveur -> création de ressources

## 3] Gestion du cache

	GET / HTTP/1.1
	Host : localhost

## 4] En pratique

* a.
  
	  curl -G --http1.1 -I -v http://graphql.org/code/
	  
	  curl --http1.1  -v http://httpbin.org/post -d foo=bar&toto=tutu

* b.
	  
    curl -I -v --header 'If-Modified-Since: Wed, 21 Feb 2018 10:10:24 GMT' http://localhost
	  
		  HTTP/1.1 304 Not Modified
		  
		  Date: Wed, 21 Feb 2018 13:42:09 GMT
		  
		  Server: Apache/2.4.25 (Debian)
		  
		  ETag: "129e-55cd491e0bcf2"
		  
* c. 
        
        curl -I http://www.univ-lille1.fr
 	    
 	    HTTP/1.1 301 Moved Permanently
     	
     	Date: Wed, 21 Feb 2018 13:58:29 GMT
    	
    	Server: Apache/2.4.10 (Debian)
    	
    	Location: https://www.univ-lille.fr
    	
    	Content-Type: text/html; charset=iso-8859-1
	

	    curl -I http://www.univ-lille.fr



package fr.ulille.iut.m4102;

public class Calculatrice {
	
    public float addition(float a, float b) {
        return a + b;
    }

    public float soustraction(float a, float b) {
        return a - b;
    }

    public float division(float a, float b) throws DivisionParZero { 
        if ( isZero(b) ) {
            throw new DivisionParZero("dénominateur nul");
        }
        return a / b;
    }

    public float multiplication(float a, float b) {
        return a * b;
    }
    
    private boolean isZero(float n) {
    	return n == 0.0;
    }
}

class DivisionParZero extends Exception {

    public DivisionParZero(String message) {
        super(message);
    }
}

package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TalonServeur implements Runnable {

	private Socket client;

	public TalonServeur(Socket c) {
		client = c;
	}

	public void run() {
		PrintWriter envoi = null;
		BufferedReader reception = null;
		float a = 0;
		float b = 0;
		try {
			envoi = new PrintWriter(client.getOutputStream(), true);
			reception = new BufferedReader(new InputStreamReader(client.getInputStream()));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		while (true) {
			try {
				String message = reception.readLine();
				if (message != null) {
					Float rep = (float) 0.0;

					String t[] = message.split(" ");

					a = Float.valueOf(t[1]);
					b = Float.valueOf(t[2]);
					switch (t[0]) {
					case "plus":
						rep = new Calculatrice().addition(a, b);
						break;
					case "moin":
						rep = new Calculatrice().soustraction(a, b);
						break;
					case "fois":
						rep = new Calculatrice().multiplication(a, b);
						break;
					case "div":
						rep = new Calculatrice().division(a, b);
						break;
					}

					envoi.println(rep);
				}else {
					client.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}

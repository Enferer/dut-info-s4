package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TalonClient {
    private Socket clientSocket = null;
    private PrintWriter envoi = null;
    private BufferedReader reception = null;
    
    public TalonClient(String host, int port) {
    	
		try {
		    clientSocket = new Socket(host, port);
		    envoi = new PrintWriter(clientSocket.getOutputStream(), true);
		    reception = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		}catch (Exception e) {
		    e.printStackTrace();
		    return;
		}
		
	}
    
    public String envoyer(String message) {	    
		try {
			envoi.println(message);
		    return reception.readLine();
		} catch (IOException e) {
		    e.printStackTrace();
		    return null;
		}
    }
    
    public float addition(double d, double e) {
    	String reponse = envoyer("plus "+d+" "+e);
    	return Float.parseFloat(reponse);
    }
    public float soustraction(double a, double b) {
    	String reponse = envoyer("moin "+a+" "+b);
    	return Float.parseFloat(reponse);
    }
    public float multiplication(double a, double b) {
    	String reponse = envoyer("fois "+a+" "+b);
    	return Float.parseFloat(reponse);
    }

    public float division(double a, double b) throws DivisionParZero {
    	String reponse = envoyer("div "+a+" "+b);
    	return Float.parseFloat(reponse);
    }

}

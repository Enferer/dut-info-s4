package fr.ulille.iut.m4102;

public class Client {
	
    public static void main(String[] args) throws DivisionParZero {
		
    	TalonClient client = new TalonClient(args[0], Integer.parseInt(args[1]));
    	
		float plus = client.addition(2.0, 42.0);
		System.out.println("Addition de 2.0 et 42.0 : "+ plus);
	    float moin = client.soustraction(42,2);
	    System.out.println("Soustraction de 42 et 2 : "+ moin);
	    float fois =client.multiplication(2, 42);
	    System.out.println("Multiplication de 2 et 42 : "+ fois);
	    float div = client.division(42, 2);
	    System.out.println("Addition de 42 et 2 : "+ div);
    
    }
}

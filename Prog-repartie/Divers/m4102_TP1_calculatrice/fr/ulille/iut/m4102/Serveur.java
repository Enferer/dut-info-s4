package fr.ulille.iut.m4102;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {
	private ServerSocket serveurSocket = null;

	public Serveur(int port) {
		try {
			serveurSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	public void miseEnService() throws IOException {

		Socket unClient = null;

		while (true) {
			try {
				unClient = serveurSocket.accept();

			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			new Thread(new TalonServeur(unClient)).start();
		}
	}


	public static void main(String[] args) throws IOException {
		Serveur serveur = new Serveur(Integer.valueOf(args[0]));
		serveur.miseEnService();
	}
}
